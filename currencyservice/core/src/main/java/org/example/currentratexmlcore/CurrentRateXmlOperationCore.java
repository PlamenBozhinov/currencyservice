package org.example.currentratexmlcore;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.example.currencyupdatecreator.CurrencyUpdateCreator;
import org.example.errors.ExistingRequestError;
import org.example.models.CurrencyUpdate;
import org.example.operations.base.OperationError;
import org.example.operations.getcurrentxml.CurrentRateXmlInput;
import org.example.operations.getcurrentxml.CurrentRateXmlOperation;
import org.example.operations.getcurrentxml.CurrentRateXmlResult;
import org.example.rabbitmq.RabbitMQSender;
import org.example.repository.CurrencyUpdateRepository;
import org.example.validation.Validator;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CurrentRateXmlOperationCore implements CurrentRateXmlOperation {
    private final CurrencyUpdateCreator currencyUpdateCreator;
    private final Validator validator;
    private final RabbitMQSender sender;
    @Override
    public Either<OperationError, CurrentRateXmlResult> process(CurrentRateXmlInput input) {
        if (validator.checkHistoryExistence(input.getCommandId(), "XML Service", input.getCustomerId())) {
            return Either.left(new ExistingRequestError(400, "This request already exist"));
        }
        String message = "Processed rates for " + input;
        sender.sendMessage(message);

        return Either.right(CurrentRateXmlResult.builder().currencyUpdate(currencyUpdateCreator.buildCurrencyUpdate(input.getCurrency())).build());

    }
}
