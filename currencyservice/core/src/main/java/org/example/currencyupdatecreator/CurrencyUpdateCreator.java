package org.example.currencyupdatecreator;

import lombok.AllArgsConstructor;
import org.example.models.CurrencyUpdate;
import org.example.repository.CurrencyUpdateRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Component
@AllArgsConstructor
public class CurrencyUpdateCreator {
    private final CurrencyUpdateRepository currencyUpdateRepository;
    @Cacheable(value = "exchangeRates", key = "#currency")
    public CurrencyUpdate buildCurrencyUpdate(String currency) {
        return currencyUpdateRepository.getLatestCurrencyUpdate(currency);
    }
    @Cacheable(value = "exchangeRates", key = "#period + #currency")
    public List<CurrencyUpdate> buildCurrencyUpdateHistory(String period, String currency) {
        Instant endTime = Instant.now().minus(Long.parseLong(period), ChronoUnit.HOURS);
        return currencyUpdateRepository.getCurrencyUpdateByPeriod(endTime,currency);
    }
}
