package org.example.validation;

import lombok.AllArgsConstructor;
import org.example.models.HistoryRequest;
import org.example.repository.CurrencyUpdateRepository;
import org.example.repository.HistoryRepository;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Objects;

@Component
@AllArgsConstructor
public class Validator {

    private final HistoryRepository historyRepository;

    public Boolean checkHistoryExistence(String requestId, String serviceName, String clientId) {
        HistoryRequest historyRequest = historyRepository.findHistoryRequestByRequestIdIs(requestId);
        if (Objects.nonNull(historyRequest)) {
            return Boolean.TRUE;
        }
        historyRepository.save(HistoryRequest.builder()
                .requestId(requestId)
                .clientId(clientId)
                .timestamp(Instant.now())
                .serviceName(serviceName)
                .build());
        return Boolean.FALSE;
    }

}
