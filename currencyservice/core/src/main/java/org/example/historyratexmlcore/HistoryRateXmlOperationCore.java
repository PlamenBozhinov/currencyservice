package org.example.historyratexmlcore;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.example.currencyupdatecreator.CurrencyUpdateCreator;
import org.example.errors.ExistingRequestError;
import org.example.operations.base.OperationError;
import org.example.operations.gethistoryxml.HistoryRateXmlInput;
import org.example.operations.gethistoryxml.HistoryRateXmlOperation;
import org.example.operations.gethistoryxml.HistoryRateXmlResult;
import org.example.rabbitmq.RabbitMQSender;
import org.example.validation.Validator;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HistoryRateXmlOperationCore implements HistoryRateXmlOperation {

    private final CurrencyUpdateCreator currencyUpdateCreator;
    private final Validator validator;
    private final RabbitMQSender sender;

    @Override
    public Either<OperationError, HistoryRateXmlResult> process(HistoryRateXmlInput input) {
        if (validator.checkHistoryExistence(input.getCommandId(), "XML Service", input.getCustomerId())) {
            return Either.left(new ExistingRequestError(400, "This request already exist"));
        }

        String message = "Processed rates for " + input;
        sender.sendMessage(message);

        return Either.right(HistoryRateXmlResult.builder().currencyUpdateList(currencyUpdateCreator.buildCurrencyUpdateHistory(input.getPeriod(), input.getCurrency())).build());
    }
}
