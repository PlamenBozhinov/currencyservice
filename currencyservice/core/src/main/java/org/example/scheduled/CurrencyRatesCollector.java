package org.example.scheduled;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.example.models.CurrencyUpdate;
import org.example.models.FixerResponse;
import org.example.models.Rate;
import org.example.rabbitmq.RabbitMQSender;
import org.example.repository.CurrencyUpdateRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class CurrencyRatesCollector {
    private final ObjectMapper mapper;
    private final CurrencyUpdateRepository repository;
    private final RabbitMQSender sender;


    @Async
    @Scheduled(cron = "0 0 * * * *")
    @Transactional
    @CacheEvict(value = "currencyCache", key = "'latestCurrencyUpdate'")
    public void updateCurrencyPairs() {

        String apiKey = "59b295df093dce7bc44fe3fe3b68b76a";
        String baseUrl = "http://data.fixer.io/api/latest";
        // Currencies you want to get
        List<String> baseCurrencies = Collections.singletonList("EUR");
        String symbols = "USD,GBP,CAD,CHF,CNY";
        baseCurrencies.forEach(c -> {

            try {
                //  URL url = new URL(baseUrl + "?access_key=" + apiKey + "&base=" + c + "&symbols=" + symbols);
                URL url = new URL(baseUrl + "?access_key=" + apiKey + "&symbols=" + symbols);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");

                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {

                    FixerResponse fixerIOResponse = mapper.readValue(connection.getInputStream(), FixerResponse.class);
                    CurrencyUpdate currencyUpdate = CurrencyUpdate.builder()
                            .time(Instant.ofEpochSecond(Long.parseLong(fixerIOResponse.getTimestamp())))
                            .build();
                    List<Rate> rateList = new ArrayList<>();
                    fixerIOResponse.getRates().entrySet().forEach(e -> {

                        rateList.add(Rate.builder()
                                .firstPair(fixerIOResponse.getBase())
                                .secondPair(e.getKey())
                                .value(e.getValue())
                                .currencyUpdate(currencyUpdate)
                                .build());
                    });
                    currencyUpdate.setRateList(rateList);
                    repository.save(currencyUpdate);
                    String message = "Processed rates for " + currencyUpdate;
                    sender.sendMessage(message);
                }

                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}

