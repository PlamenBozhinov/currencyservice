package org.example.historyratejsoncore;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.example.currencyupdatecreator.CurrencyUpdateCreator;
import org.example.errors.ExistingRequestError;
import org.example.operations.base.OperationError;
import org.example.operations.gethistoryjson.HistoryRateJsonInput;
import org.example.operations.gethistoryjson.HistoryRateJsonOperation;
import org.example.operations.gethistoryjson.HistoryRateJsonResult;
import org.example.rabbitmq.RabbitMQSender;
import org.example.validation.Validator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class HistoryRateJsonOperationCore implements HistoryRateJsonOperation {
    private final CurrencyUpdateCreator currencyUpdateCreator;
    private final Validator validator;
    private final RabbitMQSender sender;

    @Override
    public Either<OperationError, HistoryRateJsonResult> process(HistoryRateJsonInput input) {
        if (validator.checkHistoryExistence(input.getId(), "JSON Service", input.getClientId())) {
            return Either.left(new ExistingRequestError(400, "This request already exist"));
        }
        String message = "Processed rates for " + input;
        sender.sendMessage(message);

        return Either.right(HistoryRateJsonResult.builder().currencyUpdateList(currencyUpdateCreator.buildCurrencyUpdateHistory(input.getPeriod(), input.getCurrency())).build());
    }
}
