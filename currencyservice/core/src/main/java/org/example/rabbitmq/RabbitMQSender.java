package org.example.rabbitmq;

import org.springframework.stereotype.Service;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class RabbitMQSender {
    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;

    @Autowired
    public RabbitMQSender(RabbitTemplate rabbitTemplate, Queue queue) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
    }

    public void sendMessage(String message) {
        System.out.println("Send message: " + message);
        rabbitTemplate.convertAndSend(queue.getName(), message);
    }
}
