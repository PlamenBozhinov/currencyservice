package org.example.currentratecore;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.example.currencyupdatecreator.CurrencyUpdateCreator;
import org.example.errors.ExistingRequestError;
import org.example.operations.base.OperationError;
import org.example.operations.getcurrentjson.CurrentRateJsonInput;
import org.example.operations.getcurrentjson.CurrentRateJsonOperation;
import org.example.operations.getcurrentjson.CurrentRateJsonResult;
import org.example.rabbitmq.RabbitMQSender;
import org.example.validation.Validator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CurrentRateJsonOperationCore implements CurrentRateJsonOperation {

    private final CurrencyUpdateCreator currencyUpdateCreator;
    private final Validator validator;
    private final RabbitMQSender sender;

    @Override
    public Either<OperationError, CurrentRateJsonResult> process(CurrentRateJsonInput input) {
        if (validator.checkHistoryExistence(input.getId(), "JSON Service", input.getClientId())) {
            return Either.left(new ExistingRequestError(400, "This request already exist"));
        }

        String message = "Processed rates for " + input;
        sender.sendMessage(message);

        return Either.right(CurrentRateJsonResult.builder().currencyUpdate(currencyUpdateCreator.buildCurrencyUpdate(input.getCurrency())).build());
    }

}
