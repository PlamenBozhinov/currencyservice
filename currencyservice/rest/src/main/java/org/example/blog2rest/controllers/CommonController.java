package org.example.blog2rest.controllers;

import io.vavr.control.Either;
import org.example.operations.base.OperationError;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class CommonController {
    protected ResponseEntity<?> handleResult(Either<OperationError, ?> result, MediaType mediaType) {
        if (result.isRight()) {
            return ResponseEntity.status(200)
                    .body(result.get());
        }
        final OperationError operationError = result.getLeft();
        return ResponseEntity
                .status(operationError.getErrorCode())
                .contentType(mediaType)
                .body(operationError.getErrorMessage());
    }
}