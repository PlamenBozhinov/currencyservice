package org.example.blog2rest.controllers;

import lombok.AllArgsConstructor;
import org.example.operations.getcurrentjson.CurrentRateJsonInput;
import org.example.operations.getcurrentjson.CurrentRateJsonOperation;
import org.example.operations.gethistoryjson.HistoryRateJsonInput;
import org.example.operations.gethistoryjson.HistoryRateJsonOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/currency")
@AllArgsConstructor
public class CurrencyRateJsonController extends CommonController{
private final CurrentRateJsonOperation currentRateOperation;
private final HistoryRateJsonOperation historyRateJsonOperation;

    @PostMapping("/json_api/current")
    public ResponseEntity<?> getCurrentRatesJson(@RequestBody CurrentRateJsonInput input){
        return handleResult(currentRateOperation.process(input),MediaType.APPLICATION_JSON);
    }
    @PostMapping("/json_api/history")
    public ResponseEntity<?> getHistoryRatesJson(@RequestBody HistoryRateJsonInput input){
        return handleResult(historyRateJsonOperation.process(input), MediaType.APPLICATION_JSON);
    }

}
