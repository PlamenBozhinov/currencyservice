package org.example.blog2rest.controllers;

import lombok.AllArgsConstructor;
import org.example.operations.getcurrentxml.CurrentRateXmlInput;
import org.example.operations.getcurrentxml.CurrentRateXmlOperation;
import org.example.operations.gethistoryxml.HistoryRateXmlInput;
import org.example.operations.gethistoryxml.HistoryRateXmlOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/currency")
@AllArgsConstructor
public class CurrencyRateXmlController extends CommonController {
    private final CurrentRateXmlOperation currentRateXmlOperation;
    private final HistoryRateXmlOperation historyRateXmlOperation;

    @PostMapping(value = "/xml_api/command",
            consumes = "application/xml",
            produces = "application/xml")
    public ResponseEntity<?> getCurrentRatesXml(@RequestBody CurrentRateXmlInput input){
        return handleResult(currentRateXmlOperation.process(input), MediaType.APPLICATION_XML);
    }
    @PostMapping(value = "/xml_api/history",
            consumes = "application/xml",
            produces = "application/xml")
    public ResponseEntity<?> getHistoryRatesXml(@RequestBody HistoryRateXmlInput input){
        return handleResult(historyRateXmlOperation.process(input), MediaType.APPLICATION_XML);
    }
}
