package org.example.repository;

import org.example.models.HistoryRequest;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface HistoryRepository extends JpaRepository<HistoryRequest, Long> {
    HistoryRequest findHistoryRequestByRequestIdIs(String id);
}
