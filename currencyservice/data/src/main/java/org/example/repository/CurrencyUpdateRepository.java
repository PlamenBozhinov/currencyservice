package org.example.repository;

import org.example.models.CurrencyUpdate;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;


public interface CurrencyUpdateRepository extends JpaRepository<CurrencyUpdate, Long> {

    @Query(value = "SELECT cu FROM CurrencyUpdate cu JOIN fetch cu.rateList r where r.firstPair = ?1 ORDER BY cu.time DESC limit 1")
    CurrencyUpdate getLatestCurrencyUpdate(String currency);


    @Query("SELECT cu FROM CurrencyUpdate cu JOIN FETCH cu.rateList r WHERE r.firstPair = ?2 AND cu.time >= ?1")
    List<CurrencyUpdate> getCurrencyUpdateByPeriod(Instant endTime, String currency);
}
