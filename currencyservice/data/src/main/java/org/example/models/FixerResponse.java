package org.example.models;

import lombok.*;

import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@Builder
public class FixerResponse {
    private String base;
    private String date;
    private Map<String, Double> rates;
    private boolean success;
    private String timestamp;
}
