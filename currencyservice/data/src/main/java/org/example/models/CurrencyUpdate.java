package org.example.models;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "currency_update")
public class CurrencyUpdate implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "update_time")
    private Instant time;

    @OneToMany(mappedBy = "currencyUpdate", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    List<Rate> rateList;

}
