package org.example.models;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "history_requests")
public class HistoryRequest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "request_id")
    String requestId;

    @Column(name = "client_id")
    String clientId;

    @Column(name = "service_name")
    String serviceName;

    @Column(name = "time")
    Instant timestamp;
}
