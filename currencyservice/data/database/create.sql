create table currency_update
(
    id          int auto_increment
        primary key,
    update_time timestamp null
);

create table history_requests
(
    id           int auto_increment
        primary key,
    request_id   varchar(20) null,
    client_id    varchar(20) null,
    time         timestamp   not null,
    service_name varchar(40) not null
);

create table rates
(
    first_pair         varchar(10) not null,
    second_pair        varchar(10) not null,
    value              double      null,
    currency_update_id int         null,
    id                 int auto_increment
        primary key,
    constraint FK_rates_currency_update
        foreign key (currency_update_id) references currency_update (id)
);