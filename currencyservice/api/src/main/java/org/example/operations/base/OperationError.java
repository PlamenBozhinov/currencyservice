package org.example.operations.base;

public interface OperationError {
    String getErrorMessage();
    Integer getErrorCode();
}
