package org.example.operations.gethistoryxml;

import org.example.operations.base.Operation;

public interface HistoryRateXmlOperation extends Operation<HistoryRateXmlInput, HistoryRateXmlResult> {
}
