package org.example.operations.getcurrentxml;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.example.models.CurrencyUpdate;
import org.example.operations.base.OperationResult;

import java.io.Serializable;

@AllArgsConstructor
@Builder
@Getter
@Setter
public class CurrentRateXmlResult implements OperationResult, Serializable {
    CurrencyUpdate currencyUpdate;
}
