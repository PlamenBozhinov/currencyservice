package org.example.operations.getcurrentjson;

import lombok.*;
import org.example.operations.base.OperationInput;

import java.io.Serializable;

@Data
@ToString
public class CurrentRateJsonInput implements OperationInput, Serializable {
    String id;
    String timestamp;
    String clientId;
    String currency;
}
