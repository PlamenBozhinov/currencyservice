package org.example.operations.base;

import io.vavr.control.Either;

public interface Operation<I extends OperationInput, R extends OperationResult> {
    Either<OperationError,R> process(I input);
}
