package org.example.operations.gethistoryjson;

import org.example.operations.base.Operation;

public interface HistoryRateJsonOperation extends Operation<HistoryRateJsonInput, HistoryRateJsonResult> {
}
