package org.example.operations.gethistoryjson;

import lombok.Data;
import lombok.ToString;
import org.example.operations.base.OperationInput;

import java.io.Serializable;

@Data
@ToString
public class HistoryRateJsonInput implements OperationInput, Serializable {
    String id;
    String timestamp;
    String clientId;
    String currency;
    String period;
}
