package org.example.operations.getcurrentjson;

import org.example.operations.base.Operation;

public interface CurrentRateJsonOperation extends Operation<CurrentRateJsonInput, CurrentRateJsonResult> {

}
