package org.example.operations.getcurrentxml;

import lombok.Data;
import lombok.ToString;
import org.example.operations.base.OperationInput;

import java.io.Serializable;

@Data
@ToString
public class CurrentRateXmlInput implements OperationInput, Serializable {
    String commandId;
    String customerId;
    String currency;
}
