package org.example.operations.gethistoryxml;

import lombok.Data;
import lombok.ToString;
import org.example.operations.base.OperationInput;

import java.io.Serializable;

@Data
@ToString
public class HistoryRateXmlInput implements OperationInput, Serializable {
    String commandId;
    String customerId;
    String currency;
    String period;
}
