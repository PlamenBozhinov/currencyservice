package org.example.operations.getcurrentxml;

import org.example.operations.base.Operation;

public interface CurrentRateXmlOperation extends Operation<CurrentRateXmlInput, CurrentRateXmlResult> {
}
