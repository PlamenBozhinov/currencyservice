package org.example.operations.gethistoryjson;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.example.models.CurrencyUpdate;
import org.example.operations.base.OperationResult;

import java.io.Serializable;
import java.util.List;
@AllArgsConstructor
@Builder
@Getter
@Setter
public class HistoryRateJsonResult implements OperationResult, Serializable {
    List<CurrencyUpdate> currencyUpdateList;
}
