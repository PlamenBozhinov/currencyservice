package org.example.errors;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.example.operations.base.OperationError;

@Data
@AllArgsConstructor
@Builder
public class GeneralErrors implements OperationError {
    private final Integer errorCode;
    private final String errorMessage;
}
