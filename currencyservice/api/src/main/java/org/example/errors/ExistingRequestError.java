package org.example.errors;

import java.io.Serializable;

public class ExistingRequestError extends GeneralErrors implements Serializable {
    public ExistingRequestError(Integer errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }
}
